# ! /bin/bash
# Programa Menu de opciones
# Autor: Camilo Barrera Bolivar

opcion=0


while :
do
    #Limpiar la pantalla 
    clear
    #Desplegar el menú
    echo "---------------------------------"
    echo "PGUTIL - Programa"
    echo "---------------------------------"
    echo "       MENU PRINCIPAL            "
    echo "---------------------------------"
    echo "1. Instalar Postgresql"
    echo "2. Desinstalar Postgresql"
    echo "3. Respaldo"
    echo "4. Restaurar Respaldo"
    echo "5. Salir"

    #Leer datos del usuario2
    read -n1  -p "Ingrese una opción [1-5]" opcion

    #Validar opción
    case $opcion in 
        1) 
            echo "Instalando postgresql..."
            sleep 1
            ;;
        2) 
            echo "Desinstalar postgresql..."
            sleep 1
            ;;
        3) 
            echo "Sacar respaldo..."
            sleep 1
            ;;
        4) 
            echo "Restaurar respaldo..."
            sleep 1
            ;;
        5) 
            echo "Salir del programa"
            exit 0
            ;;
   esac
done 
