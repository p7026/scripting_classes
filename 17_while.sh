# ! /bin/bash
# Programa para el uso de la iteración while
# Autor: Camilo Barrera Bolivar

num=1

while [ $num -ne 10 ]
do
    echo "Imprimiendo $num veces"
    num=$((num + 1))
done
    
