# ! /bin/bash
# Programa para el uso de la iteración for
# Autor: Camilo Barrera Bolivar

arrNum=(1 2 3 4 5 6)
arrCad=(Anderson, Camilo, Barrera, Bolivar)
arrRan=({A..Z} {10..20} )

echo "Iterar en la lista de Números"
for num in ${arrNum[*]}
do
    echo "Número: $num"
done

echo "Iterar en la lista de Cadenas"
for nom in "Marco" "Pedro" "Luis" "Daniela"
do 
    echo "Nombres:$nom"
done

echo "Iterar archivos"
for fill in *
do 
    echo "Nombre archivo:$fill"
done

echo "Iterar utilizando un comando"
for fill in $(ls)
do
    echo "Nombre archivo:$fill"
done

echo "Iterar utilizando el formato three expression"
for ((i=0;i<10;i++))
do
echo "Número:$i"
done





