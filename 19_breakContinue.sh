# ! /bin/bash
# Programa para el uso de break and continue
# Autor: Camilo Barrera Bolivar

echo "Break and continue"

for fill in $(ls)
do
    for nombre in {1..4}
    do
        if [ $fill = "10_options.sh"  ]; then
            break;
        elif [[ $fill == 4*  ]]; then
            continue;
        else 
            echo "Nombre Archivo:$fill _ $nombre "
        fi
    done
done

    
