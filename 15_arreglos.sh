# ! /bin/bash
# Programa para el uso de los arreglos
# Autor: Camilo Barrera Bolivar

arrNum=(1 2 3 4 5 6)
arrCad=(Anderson, Camilo, Barrera, Bolivar)
arrRan=({A..Z} {10..20} )

#Imprimir los valores de los arreglos
echo "Arreglo de Números:${arrNum[*]}"
echo "Arreglo de Cadenas:${arrCad[*]}"
echo "Arreglo de Rangos:${arrRan[*]}"

#Imprimir los tamaños de los arreglos
echo "Arreglo de Números:${#arrNum[*]}"
echo "Arreglo de Cadenas:${#arrCad[*]}"
echo "Arreglo de Rangos:${#arrRan[*]}"

#Imprimir la posición 3 del arreglo de números, cadenas de rango
echo "Arreglo de Números:${arrNum[3]}"
echo "Arreglo de Cadenas:${arrCad[3]}"
echo "Arreglo de Rangos:${arrRan[3]}"

#Añadir y elimnar valores en un arreglo
arrNum[6]=20
unset arrNum[0]
echo "Arreglo de Números:${arrNum[*]}"
echo "Tamaño de arreglo de Números ${#arrNum[*]}"
