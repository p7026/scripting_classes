# ! /bin/bash
# Programa para trabajar operadores
# Autor: Camilo Barrera Bolivar

numA=24
numB=7

echo "Operadores Aritmeticos"
echo "Numero A: $numA  & Numero B: $numB"
echo "Sumar A + B = "  $((numA + numB))
echo "Restar  A - B = " $((numA - numB))
echo "Multiplicar A * B = " $((numA * numB))
echo "Dividir A / B = " $((numA / numB))
echo "Residuo  A + B = "$((numA % numB))


echo -e  "\n Operadores Relacionales"
echo "Numero A: $numA  & Numero B: $numB"
echo " A > B =" $((numA > numB))
echo " A < B =" $((numA < numB))
echo " A >= B =" $((numA >= numB))
echo " A >= B =" $((numA >= numB))
echo " A == B =" $((numA == numB))
echo " A != B =" $((numA != numB))

echo -e  "\n Operadores Asignación"
echo "Numero A: $numA  & Numero B: $numB"
echo "Sumar  A+= B" $((numA += numB))
echo "Restar A -=  B =" $((numA -= numB))
echo " Multiplicar *= B =" $((numA *= numB))
echo "Dvidir A /=  B =" $((numA /= numB))
