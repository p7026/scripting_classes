# ! /bin/bash
# Programa para Ejemplificar empaquetamiento con tar y pbzip
# Autor: Camilo Barrera Bolivar

echo "Empaquetar todos los scripts shellCourse"
tar -cvf shellCourse.tar *.sh
sudo apt-get install pbzip2
pbzip2 -f shellCourse.tar

echo "Empaquetar un directorio con tar y pbzip2"
tar -cf *.sh -c > shellCourse.tar.bz2
