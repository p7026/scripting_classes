# ! /bin/bash
# Programa para leer en un archivo
# Autor: Camilo Barrera Bolivar

echo "Leer  dentro de un archivo"

cat $1
echo -e "\n Almacenar los valores en una variables "
valorCat=`cat $1`
echo "$valorCat"

#Se utiliza la variable especial IFS ( Internal Field Separator ) para evitar que los espacios en blanco se recorten

echo -e "\n Leer archivos linea por linea utilizando while"
while IFS= read linea
do 
    echo "$linea"
done < $1

