# ! /bin/bash
# Programa para Ejemplificar las operaciones
# Autor: Camilo Barrera Bolivar

echo "Operaciones de un archivo"

mkdir -m 755 backupScripts

echo -e "\n Copiar los script del directorio actua¿l al nuevo directorio "
cp *.* backupScripts/
ls -la backupScripts/

echo -e "\n Mover el directorio de backupScript a otra ubicación: $HOME"

mv backupScripts $HOME

echo -e "\n Eliminar los archivos .txt"
rm *.txt

