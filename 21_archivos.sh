# ! /bin/bash
# Programa para la creación de archivos y directorios
# Autor: Camilo Barrera Bolivar

echo "Archivos directorios"

if [ $1 == "d" ]; then
    mkdir -m 755 $2
    echo "Directorio creado correctamente"
    ls -la $2
elif [ $1 = "f"  ]; then
    touch $2
    echo "Archivo creado correctamente"
    ls -la $2
else
    echo "No existe we: $1"
fi

