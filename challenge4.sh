# ! /bin/bash
# Programa Menu de opciones reto 4
# Autor: Camilo Barrera Bolivar

opcion=0


while :
do
    #Limpiar la pantalla 
    clear
    #Desplegar el menú
    echo "---------------------------------"
    echo "CHALLENGE - COMANDOS BASH"
    echo "---------------------------------"
    echo "       MENU PRINCIPAL            "
    echo "---------------------------------"
    echo "1. Procesos actuales"
    echo "2. Memoria disponible"
    echo "3. Espacio de Disco"
    echo "4. Información de red"
    echo "5. Variables de entorno configuradas"
    echo "6. Información Programa"
    echo "7. Backup información"
    echo "8. Salir "

    #Leer datos del usuario2
    read -n1  -p "Ingrese una opción [1-8]" opcion

    #Validar opción
    case $opcion in 
        1) 
            echo "Procesos actuales: "
            ps -a
            sleep 3
            ;;
        2) 
            free -h
            sleep 3
            ;;
        3)
            df -h
            sleep 2
            ;;
        4) 
            ifconfing -a
            sleep 2
            ;;
        5) 
            env -u VAR
            sleep 3
            ;;
        6)
            dpkg -l | more
            sleep 3
            ;;
        7)
            tar -czvf
            sleep 3
            ;;
        8) 
            echo "Saliendo ..."
            exit 0
            ;;

   esac
done 
